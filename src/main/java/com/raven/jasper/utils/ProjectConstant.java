package com.raven.jasper.utils;

public class ProjectConstant {

    public static final String DOUBLE_QUOTE = "\"";
    public static final String CLOSE_BRACKET = "]";
    public static final String OPEN_BRACKET = "[";
    public static final String COMMA = ",";
}
