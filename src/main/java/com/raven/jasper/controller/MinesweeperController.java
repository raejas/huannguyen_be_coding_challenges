package com.raven.jasper.controller;

import com.raven.jasper.model.ResponseData;
import com.raven.jasper.service.MinesweeperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/minesweeper")
public class MinesweeperController {

    @Autowired
    private MinesweeperService service;

    @PostMapping("/execute")
    public ResponseData execute(@RequestParam String input) {
        Boolean[][] inputBoolTypes = convertToBooleanInputs(input);
        return ResponseData.of(service.execute(inputBoolTypes));
    }

    /**
     * Convert from input string to boolean array
     *
     * @param input     String of board
     * @return  boolean array of input
     */
    private Boolean[][] convertToBooleanInputs(String input) {
        String[] inputs = input.split("],");
        List<String[]> rawInputList = new ArrayList<>();
        int totalCol = 0;
        for (String item : inputs) {
            String[] items = item.replaceAll(Pattern.quote("["), "").split(",");
            rawInputList.add(items);
            totalCol = items.length;
        }

        Boolean[][] inputBoolTypes = new Boolean[rawInputList.size()][totalCol];
        for (int i = 0; i < rawInputList.size(); i++) {
            for (int j = 0; j < totalCol; j++) {
                inputBoolTypes[i][j] = Boolean.valueOf(rawInputList.get(i)[j]);
            }
        }
        return inputBoolTypes;
    }
}
