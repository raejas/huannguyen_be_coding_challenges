package com.raven.jasper.controller;

import com.raven.jasper.model.ResponseData;
import com.raven.jasper.service.LogService;
import com.raven.jasper.utils.ProjectConstant;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/callLogs")
public class LogController {

    @Autowired
    private LogService service;

    @PostMapping("/analyse")
    public ResponseData analyze(@RequestParam String logs) {
        String[] apiCallLogs = logs.replaceAll(Pattern.quote(ProjectConstant.OPEN_BRACKET), Strings.EMPTY)
                .replaceAll(Pattern.quote(ProjectConstant.CLOSE_BRACKET), Strings.EMPTY)
                .replaceAll(ProjectConstant.DOUBLE_QUOTE, Strings.EMPTY)
                .split(ProjectConstant.COMMA);

        return ResponseData.of(service.analyseApiCall(Arrays.asList(apiCallLogs)));
    }
}
