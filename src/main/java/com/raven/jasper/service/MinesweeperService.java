package com.raven.jasper.service;

import java.util.List;

public interface MinesweeperService {

    /**
     * Execute input to create new minesweeper board
     *
     * @param input boolean 2-dimension array with mine set
     * @return      minesweeper board based on list
     */
    List<String> execute(Boolean[][] input);
}
