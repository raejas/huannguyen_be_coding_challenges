package com.raven.jasper.service;

import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.List;

public interface LogService {

    /**
     * Analyze api call to calculate and build tree node
     *
     * @param apiCallList   list of api call
     * @return  tree node of api call
     */
    ObjectNode analyseApiCall(List<String> apiCallList);
}
