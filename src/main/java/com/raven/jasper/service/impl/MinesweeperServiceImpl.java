package com.raven.jasper.service.impl;

import com.raven.jasper.service.MinesweeperService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MinesweeperServiceImpl implements MinesweeperService {

    private static final String SEPERATOR = ", ";

    @Override
    public List<String> execute(Boolean[][] input) {
        List<String> output = new ArrayList<>();
        int totalRow = input.length;
        int totalColumn = input[0].length;

        for (int i = 0; i < totalRow; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < totalColumn; j++) {
                int neighborCnt = calculateNeighborCnt(input, totalRow, totalColumn, i, j);

                sb.append(neighborCnt);
                if (j < totalColumn - 1) {
                    sb.append(SEPERATOR);
                }
            }

            output.add(sb.toString());
        }

        return output;
    }

    /**
     * Calculate neighbour mines
     *
     * @param input         boolean array input
     * @param totalRow      total length of input row
     * @param totalColumn   total length of input column
     * @param i             index of current item's row
     * @param j             index of current item's column
     * @return  total count of neighbour mine
     */
    private int calculateNeighborCnt(Boolean[][] input, int totalRow, int totalColumn, int i, int j) {
        int neighbourCnt = 0;
        for (int k = Math.max(0, i - 1); k <= Math.min(i + 1, totalRow - 1); k++) {
            for (int l = Math.max(0, j - 1); l <= Math.min(j + 1, totalColumn - 1); l++) {
                if (input[k][l] && (k != i || l != j)) {
                    neighbourCnt++;
                }
            }
        }
        return neighbourCnt;
    }
}
