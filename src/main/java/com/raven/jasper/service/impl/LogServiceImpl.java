package com.raven.jasper.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.raven.jasper.service.LogService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class LogServiceImpl implements LogService {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public ObjectNode analyseApiCall(List<String> apiCallList) {
        ObjectNode treeNode = mapper.createObjectNode();
        analyse(0, apiCallList, treeNode);
        return treeNode;
    }

    /**
     * Analyze api call list recursive to append in parent node
     *
     * @param currIndex     current index to split
     * @param list          list of api call
     * @param parentNode    node of parent (project, theme)
     */
    private void analyse(int currIndex, List<String> list, ObjectNode parentNode) {
        Map<String, List<String>> itemMap = list.stream().collect(Collectors.groupingBy(item -> item.split("/")[currIndex]));
        for (Map.Entry<String, List<String>> entry : itemMap.entrySet()) {
            String key = entry.getKey();
            List<String> childList = entry.getValue();

            ObjectNode childNode = mapper.createObjectNode();
            if (currIndex < 2) {
                childNode.put("_count", childList.size());
                analyse(currIndex + 1, childList, childNode);
            } else {
                parentNode.put(key, childList.size());
                continue;
            }

            parentNode.set(key, childNode);
        }
    }
}
