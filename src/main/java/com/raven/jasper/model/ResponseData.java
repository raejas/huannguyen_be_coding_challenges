package com.raven.jasper.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseData<T> {

    private T result;

    private ResponseData(T result) {
        this.result = result;
    }

    public static <T> ResponseData of(T result) {
        return new ResponseData(result);
    }
}
