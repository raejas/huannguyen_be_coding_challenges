package com.raven.jasper.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.raven.jasper.service.LogService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class LogServiceImplTest {

    private static List<String> apiList = new ArrayList<>();

    @Autowired
    private LogService service;

    @BeforeEach
    void setUp() {
        apiList.add("unishop/product/list");
        apiList.add("unishop/category/list");
        apiList.add("unishop/product/create");
        apiList.add("kyowon/user/list");
        apiList.add("commerce/product/create");
        apiList.add("kyowon/user/create");
        apiList.add("kyowon/banner/update");
        apiList.add("unishop/category/update");
        apiList.add("commerce/product/create");
        apiList.add("unishop/product/list");
        apiList.add("unishop/category/list");
        apiList.add("commerce/product/update");
        apiList.add("unishop/product/create");
        apiList.add("kyowon/user/list");
        apiList.add("commerce/product/delete");
        apiList.add("kyowon/user/create");
        apiList.add("kyowon/banner/update");
        apiList.add("unishop/category/delete");
    }

    @Test
    void analyseApiCall() {
        ObjectNode objectNode = service.analyseApiCall(apiList);
        JsonNode kyowonNode = objectNode.get("kyowon");
        assertEquals(6, kyowonNode.get("_count").asInt());

        JsonNode bannerNode = kyowonNode.get("banner");
        assertNotNull(bannerNode);

        JsonNode bannerListNode = bannerNode.get("list");
        assertNull(bannerListNode);
    }
}