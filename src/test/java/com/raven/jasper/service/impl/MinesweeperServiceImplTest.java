package com.raven.jasper.service.impl;

import com.raven.jasper.service.MinesweeperService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class MinesweeperServiceImplTest {

    private static Boolean[][] firstInput = new Boolean[3][3];
    private static Boolean[][] secondInput = new Boolean[2][3];
    private static Boolean[][] thirdInput = new Boolean[5][2];

    @Autowired
    private MinesweeperService service;

    @BeforeEach
    void setUp() {
        firstInput = new Boolean[][]{
                {true, false, false},
                {false, true, false},
                {false, false, false}
        };

        secondInput = new Boolean[][]{
                {false, false, false},
                {false, false, false}
        };

        thirdInput = new Boolean[][]{
                {true, false},
                {true, false},
                {false, true},
                {false, false},
                {false, false}
        };
    }

    @Test
    void execute() {
        List<String> firstOutput = service.execute(firstInput);
        assertEquals("1, 2, 1", firstOutput.get(0));

        List<String> secondOutput = service.execute(secondInput);
        assertEquals("0, 0, 0", secondOutput.get(1));

        List<String> thirdOutput = service.execute(thirdInput);
        assertEquals("2, 1", thirdOutput.get(2));
    }
}