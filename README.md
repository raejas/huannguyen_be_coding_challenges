# HuanNguyen_BE_Coding_Challenges

Requirement 1:
You are given a list of API calls in the format ${project}/${theme}/${method}. You need to calculate and print the number of calls to each node of API endpoint as a tree.

Requirement 2:
In the popular Minesweeper game you have a board with some mines and those cells that don’t contain a mine have a number in it that indicates the total number of mines in the neighboring cells. Starting off with some arrangement of mines we want to create a Minesweeper game setup.
